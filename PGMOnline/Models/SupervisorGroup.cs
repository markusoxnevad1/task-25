﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PGMOnline.Models
{
    public static class SupervisorGroup
    {
        private static List<Supervisor> CurrentSupervisors = new List<Supervisor>
        {
                new Supervisor{Id=1, Name="Markus", FieldOfScience="Mathematics", YearsOfExperience=4},
                new Supervisor{Id=2, Name="John", FieldOfScience="Physical Education", YearsOfExperience=7},
                new Supervisor{Id=3, Name="Lisa", FieldOfScience="History", YearsOfExperience=8},
                new Supervisor{Id=4, Name="Snape", FieldOfScience="Defence Against the Dark Arts", YearsOfExperience=29},
                // Empty object to demonstrate null conditional operator
                new Supervisor()
        };
        //  Method that generates a list containing 5 supervisors
        public static List<Supervisor> GetSupervisorList()
        {
            List<Supervisor> CurrentSupervisors = new List<Supervisor>
            {
                new Supervisor{Id=1, Name="Markus", FieldOfScience="Mathematics", YearsOfExperience=4},
                new Supervisor{Id=2, Name="John", FieldOfScience="Physical Education", YearsOfExperience=7},
                new Supervisor{Id=3, Name="Lisa", FieldOfScience="History", YearsOfExperience=8},
                new Supervisor{Id=4, Name="Snape", FieldOfScience="Defence Against the Dark Arts", YearsOfExperience=29},
            };
            return CurrentSupervisors;
        }
        public static List<Supervisor> Supervisors
        {
            get { return CurrentSupervisors; }
        }
        public static void AddSupervisor(Supervisor newSupervisor)
        {
            CurrentSupervisors.Add(newSupervisor);
        }
    }
}
