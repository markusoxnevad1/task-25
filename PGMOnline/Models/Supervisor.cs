﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PGMOnline.Models
{
    public class Supervisor
    {
        // Id is now nullable
        //[Required(ErrorMessage = "Please enter a number")]
        //[RegularExpression("^\\d+$", ErrorMessage = "Please enter a valid number bro")]
        public int? Id { get; set; } = 99;
        [Required(ErrorMessage = "Please enter a name")]
        public string Name { get; set; } = "No Name";
        [Required(ErrorMessage = "Please enter a field of science")]
        public string FieldOfScience { get; set; } = "No Field of Science";
        // Years of Experience is nullable
        //[Required(ErrorMessage = "Please enter some years of experience")]
        //[RegularExpression("^\\d+$", ErrorMessage = "Thats not a valid number, please enter some years of experience")]
        public int? YearsOfExperience { get; set; } = 0;
    }
}
