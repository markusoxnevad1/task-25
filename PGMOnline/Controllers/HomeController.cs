﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PGMOnline.Models;

namespace PGMOnline.Controllers
{
    public class HomeController : Controller
    {
        
        public IActionResult Index()
        {
            ViewBag.WelcomeMessage = "Welcome";
            ViewBag.Time = DateTime.Now;
            return View("MySecondView");
        }
        [HttpGet]
        public IActionResult SupervisorInfo()
        {
            return View();
        }
        [HttpGet]
        public IActionResult SfilteredSupervisors()
        {
            return View(SupervisorGroup.Supervisors.Where(s => s.Name.StartsWith('S')).ToList());
        }
        [HttpPost]
        public IActionResult SupervisorInfo(Supervisor supervisor)
        {
            if (ModelState.IsValid)
            {
                SupervisorGroup.Supervisors.Add(supervisor);
                return View("AllSupervisors", SupervisorGroup.Supervisors);
            }
            else
            {
                return View();
            }
        }
        [HttpGet]
        public IActionResult AllSupervisors()
        {
            return View(SupervisorGroup.Supervisors);
        }
        public IActionResult AddValidation()
        {
            return View("AddSupervisorConfirmation");
        }

    }
}
